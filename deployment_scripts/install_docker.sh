# docker
yum -y install docker
systemctl start docker
systemctl enable docker

# docker compose
python -c "import sys
tags = [x for x in sys.argv if x.startswith('ref') 
                            and not x.split('/')[-1].startswith('doc') 
                            and not x.split('/')[-1].endswith('^{}') 
                            and not 'rc' in x.split('/')[-1]
                            and len(x.split('/')[-1].split('.')) == 3]
tags = sorted(tags, key = lambda x:(int(x.split('/')[-1].split('.')[0]), 
                                    int(x.split('/')[-1].split('.')[1]),
                                    int(x.split('/')[-1].split('.')[2])))
print('-kL "https://github.com/docker/compose/releases/download/{version}/docker-compose-$(uname -s)-$(uname -m)"  -o /usr/local/bin/docker-compose'.format(version=tags[-1].split('/')[-1]))
# .split('/')[-1] 
" `git ls-remote --tags https://github.com/docker/compose.git` | xargs -n4 curl
chmod +x /usr/local/bin/docker-compose
which docker-compose

