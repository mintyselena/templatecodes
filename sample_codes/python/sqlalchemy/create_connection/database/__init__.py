from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.automap import automap_base

class _connection_store(object):
    def __init__(self):
        self.initilized = False
        
        self.uri = None
        self.engine = None
        self.base = None
        self.tables = None
        
_store = {} # _connection_store()

class _session_context(object):
    def __init__(self, key):
        self.session = sessionmaker(bind=_store[key].engine)()
    
    def __enter__(self):
        return self.session
    
    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self.session.flush()
            self.session.commit()
        else:
            self.session.rollback()
        self.session.close()

def setup(uri, encoding='utf-8', echo=False, key='default', update=False):
    if key in _store and not update:
        raise Exception('already setup %s' % key)
    store = _connection_store()
    
    store.uri = uri
    store.engine = create_engine(store.uri,
                                 encoding=encoding,
                                 echo=echo)
    store.base = automap_base()
    store.base.prepare(store.engine, reflect=True)
    store.tables = store.base.classes
    _store[key] = store
    
def get_connection(key='default'):
    if key not in _store:
        raise Exception('database connection is not initilized.')
    
    return _session_context(key)


def get_model(key='default'):
    if key not in _store:
        raise Exception('database connection is not initilized.')
    
    return _store[key].tables


# ORM result → dictionarys
def as_dict(orm_result, RenameKeys={}, Tables=None):
    '''
    orm_result:  all(), one() 等の結果。
    RenameKeys:  {'TableName: {'ColumnName': ReplaceName}} の定義でTableName.ColumnNameをReplaceNameに置き換える。
    Tables:      参照テーブルのリスト。指定されたテーブルが結合条件の結果存在しなかった場合にカラムを空文字で埋める。

    制限事項
    * カラム名が_から始まるカラムに対応できない
    '''

    listret = []

    if not isinstance(orm_result, list):
        orm_result = [orm_result]

    def _set_value(newrecord, value, key, RenameKey):
        if key.startswith('_'):
            return

        if RenameKey.get(key) is not None:
            key = RenameKey[key]

        if newrecord.get(key) is None:
            newrecord[key] = value

    for record in orm_result:
        newrecord = {}
        if getattr(record, '__dict__', None) is None:
            appended_tables = []
            for _table in record:
                if _table is None:
                    continue
                appended_tables.append(str(_table.__table__.name))
                RenameKey = RenameKeys.get(_table.__table__.name, {})
                for key in _table.__dict__:
                    _set_value(newrecord, getattr(
                        _table, key, ''), key, RenameKey)

            if Tables is not None:
                for _append_table in Tables:
                    _append_table_name = str(_append_table.__table__.name)
                    if not _append_table_name in appended_tables:
                        for key in _append_table.__dict__.keys():
                            _set_value(newrecord, '', key, RenameKey)

        else:
            for key in record.__dict__:
                if not key.startswith('_'):
                    newrecord[key] = getattr(record, key)
        listret.append(newrecord)
    return listret
