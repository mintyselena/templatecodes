import database

# database initilize
database.setup('mysql://<user>:<password>@<host>/<schema>?charset=utf8')
database.setup('mysql://<user>:<password>@<host>/<schema>?charset=utf8', key='readonly')

def main():
    # User テーブルを全レコード取得する
    
    # with ステートメントを使う場合
    with database.get_connection() as session:
        table = database.get_model()
        query = session.query(table.User)
        records = query.all()
        for record in database.as_dict(records):
            print(record)
            
    # with ステートメントを使わない場合
    connection = database.get_connection(key='readonly')
    session = connection.session
    try:
        table = database.get_model(key='readonly')
        query = session.query(table.User)
        records = query.all()
        for record in database.as_dict(records):
            print(record)
    finally:
        session.close()
    
    
if __name__ == '__main__':
    main()
